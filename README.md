# General

This config is multiplatform for all toradex imx6 and imx7 boards we want to add

 * since imx6 and imx7 are usually well supported we have a common linux.config
   generated through imx6_imx7_defconfig available in linux-stable
 * uboot is still coming from bsp tree u-boot-toradex; we use the board specific
   build target there, e.g. you will find a uboot.colibri-imx6ull.config there
 * product.mk and product.$subtarget.mk are loaded in that order; hence subtarget
   can overload, but does not need to repeat all configs
 * for boot.scr based systems, you can again use boot.$subtarget.cmd and boot.$subtarget.scr
   or if not available we use a common "boot.scr" in case your boards can share the same
 * same goes for uboot.env and uboot.$subtarget.env
 * same logic applies for pantavisor.$subtarget.config
 * same logic applies for pantahub.$subtarget.config
 * same logic applies for image.$subtarget.config
 * same logic applies for global.$subtarget.config

## Kernel and Build trees:

Alchemy copies atom.mk to the appropriate kernel and uboot tree it uses.

Like with config files we have implemented support for making $linux.$subtarget/ directory
taking precedence over kernel/$linux/ directory when building. Same goes for $uboot.$subtarget.

# Next?

This documenattion should also move to alchemy directory.

